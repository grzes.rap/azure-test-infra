# azure-test-infra

https://docs.microsoft.com/en-us/azure/developer/terraform/get-started-cloud-shell
```
az login
./backend.sh
az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/<subscription_id>"
terraform apply -auto-approve
```