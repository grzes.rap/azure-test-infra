NS = grzesrap
IMAGE = `basename $$PWD`

tf:
	docker-compose run --rm tools-tf

ansible:
	docker-compose run --rm tools-ansible

all:
	docker-compose run --rm tools-all
