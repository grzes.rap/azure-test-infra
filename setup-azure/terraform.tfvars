location                     = "northeurope"

project_name                 = "demo-freshmail"
main_domain_name             = "demo-freshmail"

# vm_domain_name 		     = "demovm"

aks_node_count               = 1
aks_node_type                = "Standard_B2s"  # "Standard_D2_v3"
firewall_rules               = []
vnet_rules                   = []

ssh_public_key               = "./ssh/admin.pub"
