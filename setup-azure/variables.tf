variable "location"                     {}

variable "project_name"                 {}
variable "main_domain_name"             {}

# variable "vm_domain_name"               {}

variable "aks_node_count"               {}
variable "aks_node_type"                {}
variable "client_id"                    {}
variable "client_secret"                {}
variable "firewall_rules"               {}
variable "vnet_rules"                   {}

variable "ssh_public_key"               {}
variable "aks_admin_user"               {}
variable "aks_admin_pass"               {}
