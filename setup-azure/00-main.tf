provider "azurerm" {
  version = "=2.1.0"
  features {}
}

# Terraform State on Azure
terraform {
  backend "azurerm" {
    storage_account_name  = "demotstate27511"          # Storage Account
    container_name        = "demotstate"               # Blob Service Container
    key                   = "terraform.demostate"      # Key - must be unique
  }
}

resource "azurerm_resource_group" "default" {
  name        = "${var.project_name}-rg"
  location    = var.location
}
