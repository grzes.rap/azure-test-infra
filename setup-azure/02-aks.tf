resource "azurerm_kubernetes_cluster" "aks_cluster" {
  dns_prefix            = "${var.project_name}-aks"
  location              = var.location
  name                  = "${var.project_name}-aks"
  resource_group_name   = azurerm_resource_group.default.name
  kubernetes_version    = "1.18.10"

  default_node_pool {
    name                = "mainpool"
    node_count          = var.aks_node_count
    vm_size             = var.aks_node_type
    os_disk_size_gb     = 32

    availability_zones    = []
    enable_auto_scaling   = false
    enable_node_public_ip = false
    node_labels           = {}
    node_taints           = []
    tags                  = {}

    vnet_subnet_id      = azurerm_subnet.aks_subnet.id
  }

  network_profile {
    network_plugin      = "azure"
    docker_bridge_cidr  = "172.17.0.1/16"
    dns_service_ip      = "10.10.0.10"
    service_cidr        = "10.10.0.0/22"
    load_balancer_sku   = "Basic"
  }
  # AKS requires at minimum a Service Principal that has been granted the Network Contributor role
  service_principal {
    client_id           = var.client_id
    client_secret       = var.client_secret
  }
  
  linux_profile {
    admin_username = var.aks_admin_user

    ssh_key {
      key_data = "${file("${var.ssh_public_key}")}"
    }

  }

  windows_profile {
    admin_username = var.aks_admin_user
    admin_password = var.aks_admin_pass
  }

  enable_pod_security_policy      = false
  api_server_authorized_ip_ranges = []

  addon_profile {
    http_application_routing {
      enabled = false
    }

    kube_dashboard {
      enabled = false
    }

    # # Log Analytics can give us access to the K8s Cloud Controller Manager logs which are useful for troubleshooting
    # oms_agent {
    #   enabled                    = true
    #   log_analytics_workspace_id = "${azurerm_log_analytics_workspace.aks_law.id}"
    # }
  }
  role_based_access_control {
    enabled = true
  }
}

###############################################
#   Save kube-config into azure_config file   #
###############################################
resource "null_resource" "save-kube-config" {
    triggers = {
        config = "${azurerm_kubernetes_cluster.aks_cluster.kube_config_raw}"
    }
    provisioner "local-exec" {
        command = "mkdir -p ${path.module}/.kube ~/.kube && echo '${azurerm_kubernetes_cluster.aks_cluster.kube_config_raw}' > ${path.module}/.kube/azure_config && chmod 0644 ${path.module}/.kube/azure_config && cp ${path.module}/.kube/azure_config ~/.kube/config"
    }
 
    depends_on = [ "azurerm_kubernetes_cluster.aks_cluster" ]
}
