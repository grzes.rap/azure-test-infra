# virtual network
resource "azurerm_virtual_network" "main_vnet" {
  location                  = var.location
  name                      = "${var.project_name}-vnet"
  resource_group_name       = azurerm_resource_group.default.name
  address_space             = ["10.10.0.0/16"]
}

# subnet
resource "azurerm_subnet" "aks_subnet" {
  address_prefix            = "10.10.10.0/24"
  name                      = "${var.project_name}-akssubnet"
  resource_group_name       = azurerm_resource_group.default.name
  virtual_network_name      = azurerm_virtual_network.main_vnet.name
}

# public IP with domain name
resource "azurerm_public_ip" "demo_publicip" {
    name                    = "demo-publicip"
    location                = var.location
    resource_group_name     = azurerm_resource_group.default.name
    allocation_method       = "Static"
    domain_name_label       = var.main_domain_name
}
