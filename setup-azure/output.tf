data "template_file" "app_vars" {
  template = "${file("templates/app-vars.tpl")}"
  vars = {
    rg = azurerm_public_ip.demo_publicip.resource_group_name
    ip = azurerm_public_ip.demo_publicip.ip_address
    dns = azurerm_public_ip.demo_publicip.fqdn
    dns_label = azurerm_public_ip.demo_publicip.domain_name_label
  }
}

resource "null_resource" "update_app_vars" {
  triggers = {
    template = data.template_file.app_vars.rendered
  }
  provisioner "local-exec" {
    command = "echo '${data.template_file.app_vars.rendered}' > ../setup-kubernetes/host_vars/aks-demo/app.yml"
  }

#   provisioner "local-exec" {
#     command = "cd ../setup-kubernetes/ && ansible-playbook up.yml"
#   }
}

output "instance_public_ip_rg" {
  value = "${azurerm_public_ip.demo_publicip.resource_group_name}"
}

output "instance_public_ip" {
  value = "${azurerm_public_ip.demo_publicip.ip_address}"
}

output "instance_public_ip_dns" {
  value = "${azurerm_public_ip.demo_publicip.fqdn}"
}
